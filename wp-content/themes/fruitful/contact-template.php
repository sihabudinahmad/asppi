<?php /*Template Name: Contact Template */ ?>
<?php get_header(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<div class="entry-thumbnail"><?php the_post_thumbnail(); ?></div>
	<?php endif; ?>
	<?php if (!is_front_page()) {?>	
		<header class="entry-header">
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header><!-- .entry-header -->
		<?php } ?>
		<div class="entry-content">
			<div class="info-box-row clearfix">
				<div class="ffs-two-one ffs-info-box center">
					<?php

					while ( have_posts() ) : the_post(); ?> 
					<?php the_content(); ?> 
					<?php
					endwhile; 
					wp_reset_query(); 
					?>
				</div>

				<div class="ffs-two-one ffs-info-box center">
					<?php echo do_shortcode('[contact-form-7 id="94" title="Contact form 1"]'); ?>
				</div>
			</div>
		</div>




		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'fruitful' ), 'after' => '</div>' ) ); ?>
		<?php edit_post_link( __( 'Edit', 'fruitful' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

<?php get_footer();?>