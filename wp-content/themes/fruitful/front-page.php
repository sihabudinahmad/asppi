<?php
/**
 * The template for displaying Home Page.
 *
 * @package WordPress
 * @subpackage Fruitful theme
 * @since Fruitful theme 1.0
 */

get_header(); ?>

<div id="features">
	<div class="container">			
		<div class="sixteen columns">
			<div class="title">
				<h2><?php echo get_field('judul_section_2'); ?></h2>
			</div>
			<div class="info-box-row clearfix">
				<?php 	 
				$rows = get_field('konten_section_2');
				if($rows)
				{
					if ( count($rows)==1){
						foreach($rows as $row)
						{

							?>

							<div class="ffs-one-one ffs-info-box center">
								<div class="ffs-icon-box">
									<div class="icons-set">
										<div class="icon-wrap">
											<?php echo $row['icon'];?>
										</div>
									</div>
								</div>
								<div class="ffs-content-box">
									<div class="infobox-title" style="text-align:center; font-size:20px; text-transform: uppercase; "><?php echo $row['judul_'];?></div>
									<div class="infobox-text" style="text-align:center; font-size:13px; ">
										<?php echo $row['isi_row']?>
									</div>
								</div>
							</div>


							<?php }

						}

						else if (count($rows)==2){
							foreach($rows as $row)
							{

								?>

								<div class=" ffs-two-one ffs-info-box center ">
									<div class="ffs-icon-box">
										<div class="icons-set">
											<div class="icon-wrap">
												<?php echo $row['icon'];?>
											</div>
										</div>
									</div>
									<div class="ffs-content-box">
										<div class="infobox-title" style="text-align:center; font-size:20px; text-transform: uppercase; "><?php echo $row['judul_'];?></div>
										<div class="infobox-text" style="text-align:center; font-size:13px; ">
											<?php echo $row['isi_row']?>
										</div>
									</div>
								</div>


								<?php }

							}

							else if (count($rows)==3){
								foreach($rows as $row)
								{

									?>

									<div class=" ffs-three-one ffs-info-box center ">
										<div class="ffs-icon-box">
											<div class="icons-set">
												<div class="icon-wrap">
													<?php echo $row['icon'];?>
												</div>
											</div>
										</div>
										<div class="ffs-content-box">
											<div class="infobox-title" style="text-align:center; font-size:20px; text-transform: uppercase; "><?php echo $row['judul_'];?></div>
											<div class="infobox-text" style="text-align:center; font-size:13px; ">
												<?php echo $row['isi_row']?>
											</div>
										</div>
									</div>


									<?php }

								}
								else if (count($rows)==4){

									foreach($rows as $row)
									{

										?>

										<div class=" ffs-four-one ffs-info-box center ">
											<div class="ffs-icon-box">
												<div class="icons-set">
													<div class="icon-wrap">
														<?php echo $row['icon'];?>
													</div>
												</div>
											</div>
											<div class="ffs-content-box">
												<div class="infobox-title" style="text-align:center; font-size:20px; text-transform: uppercase; "><?php echo $row['judul_'];?></div>
												<div class="infobox-text" style="text-align:center; font-size:13px; ">
													<?php echo $row['isi_row']?>
												</div>
											</div>
										</div>


										<?php }

									}
								}

								?>

							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="section-tengah">
				<div class="title">
					<h2><?php echo get_field('judul_section_tengah'); ?></h2>
				</div>

				<div class="content-section">
					<?php echo get_field('konten_tengah'); ?>
				</div> 
			</div>

			<div id="event-home">
				<div class="container">			
					<div class="sixteen columns">
						<div class="title">
							<h2><?php echo get_field('judul_section_3'); ?></h2>
						</div>

						<?php 

						$args = array(
							'post_type'=> 'event',
							'posts_per_page'    => 6,
							'order_by' => 'date'
							);
						$my_query = new wp_query( $args );
						?>
						<div class="woocommerce columns-3">

							<ul class="products">
								<?php 
								if ( $my_query->have_posts() ) :
									while ( $my_query->have_posts() ) : $my_query->the_post();
								?>

								<li class="product type-product status-publish has-post-thumbnail product_cat-clothing product_cat-hoodies first instock shipping-taxable purchasable product-type-simple">
									<div class="date"><?php the_field('tanggal'); ?></div>
									<a href="<?php the_permalink(); ?>" class="">
										<?php the_post_thumbnail('medium'); ?>
										<h1><?php the_title(); ?></h1>

									</a></li>

								<?php endwhile;
								endif;
								wp_reset_query();
								?>


							</ul>


						</div>


					</div>
				</div>
			</div>

			<?php get_footer(); ?>