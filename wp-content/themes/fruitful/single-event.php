<?php get_header(); ?>
<div id="slider-container">
	<div class="main-slider-container fullwidth">
		<section class="img-event">


			<div class="flex-active-slide">
				<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
					<div class="image-single">
						<img src="<?php the_post_thumbnail_url( 'full' );   ?>" alt="single-event">
					</div>

				<?php endif; ?>


			</div>
		</div>
	</section>
</div>
</div>

<div class="container">								
	<div class="sixteen columns">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if (!is_front_page()) {?>	
				<div class="headpost">	
					<div class="info-box-row ">
						<div class="ffs-three-two ffs-info-box center ">
							<header class="entry-header">
								<h1 class="entry-title"><?php the_title(); ?></h1>
							</header><!-- .entry-header -->
						</div>
						<div class="ffs-three-one ffs-info-box center ">
							<div class="tanggal-single">
								<i class="fa fa-calendar"></i>
								<?php the_field('tanggal');?>
							</div>
						</div>

					</div>
				</div>
				<?php } ?>

				<div class="info-box-row clearfix">
					<div class="ffs-three-two ffs-info-box center leftform">

						<div class="text-konten">
							<div class="entry-content">
								<?php

								while ( have_posts() ) : the_post(); 
								the_content(); 

								endwhile; 
								wp_reset_query(); 
								?>
							</div><!-- .entry-content -->
							<div class="alamat">
								<i class="fa fa-map-marker"></i>
								<?php echo get_field('alamat'); ?>
							</div>
						</div>
					</div>
					<div class="ffs-three-one ffs-info-box center sideform">
						<h3 class="title-side">Login untuk membership</h3>
						<?php wp_login_form(); ?>
						<h3 class="title-side">Selain membership Isilah data di sini: </h3>
						<form>
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Peserta</label>
								<input type="text" class="form-control"  placeholder="Enter nama">

							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<textarea class="form-control" id="alamat" rows="3"></textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Email address</label>
								<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
								<small id="emailHelp" class="form-text text-muted">masukkan email unik aktif anda</small>
							</div>
							<fieldset class="form-group">
								<label>Hotel</label>
								<?php
								if( have_rows('hotel') ):
									while ( have_rows('hotel') ) : the_row();
								?>
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="nama-hotel" id="nama-hotel" value="<?php the_sub_field('nama_hotel'); ?>" checked>
										<?php the_sub_field('nama_hotel'); ?>
									</label>
								</div>
								<fieldset class="form-group kamar">
									<select class="form-control" id="penjemputan">
									<option value="0">Pilih Kamar</option>
										<?php if(have_rows('kamar')):
										while (have_rows('kamar')): the_row(); ?>
										<option value="<?php the_sub_field('nomor_kamar'); ?>">
											<?php the_sub_field('nomor_kamar'); ?></option>
										<?php endwhile;
										else:
											echo "tidak tersedia lokasi penjemputan";
										endif;
										?>
									</select>

								</fieldset>
								<?php 
								endwhile;
								else :
									echo "Maaf Tidak Ada hotel yang tersedia";
								endif;
								?>
							</fieldset>

							<div class="form-group">
								<label for="penjemputan">Penjemputan</label>
								<select class="form-control" id="penjemputan">
									<option value="0">Pilih Lokasi</option>
									<?php if(have_rows('penjemputan')):
									while (have_rows('penjemputan')): the_row(); ?>
									<option value="<?php the_sub_field('lokasi_penjemputan'); ?>">
										<?php the_sub_field('lokasi_penjemputan'); ?></option>
									<?php endwhile;
									else:
										echo "tidak tersedia lokasi penjemputan";
									endif;
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="pengantaran">Pengantaran</label>
								<select class="form-control" id="pengantaran">
									<option value="0">Pilih Lokasi</option>
									<?php if(have_rows('pengantaran')):
									while (have_rows('pengantaran')): the_row(); ?>
									<option value="<?php the_sub_field('lokasi_pengantaran'); ?>">
										<?php the_sub_field('lokasi_pengantaran'); ?></option>
									<?php endwhile;
									else:
										echo "tidak tersedia lokasi pengantaran";
									endif;
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="pengantaran">Jenis Kendaraan</label>
								<select class="form-control" id="pengantaran">
									<option value="0">Pilih Kendaraan</option>
									<?php if(have_rows('jenis_kendaraan')):
									while (have_rows('jenis_kendaraan')): the_row(); ?>
									<option value="<?php the_sub_field('kendaraan'); ?>">
										<?php the_sub_field('kendaraan'); ?></option>
									<?php endwhile;
									else:
										echo "tidak tersedia kendaraan";
									endif;
									?>
								</select>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</article><!-- #post-<?php the_ID(); ?> --> 
			</div>
		</div>



		<?php 

		get_footer(); ?>