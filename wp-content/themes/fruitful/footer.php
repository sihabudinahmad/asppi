<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Fruitful theme
 * @since Fruitful theme 1.0
 */
?>
</div>
</div>
</div><!-- .page-container-->
<div id="above-footer">
	<div class="container">
		<div class="sixteen columns">
			<div class="info-box-row clearfix" id="info-box-row-86">
				<?php 	 
				$rows = get_field('above_footer','option');
				if($rows)
				{
					if ( count($rows)==1){
						foreach($rows as $row)
						{
							?>
							<div class=" ffs-one-one ffs-info-box center ">
								<div class="ffs-content-box">
									<?php echo $row['widget_kolom']?>
								</div>
							</div>
							<?php }

						}
						if ( count($rows)==2){
							foreach($rows as $row)
							{
								?>
								<div class=" ffs-two-one ffs-info-box center ">
									<div class="ffs-content-box">
										<?php echo $row['widget_kolom']?>
									</div>
								</div>
								<?php }

							}
							if ( count($rows)==3){
								foreach($rows as $row)
								{
									?>
									<div class=" ffs-three-one ffs-info-box center ">
										<div class="ffs-content-box">
											<?php echo $row['widget_kolom']?>
										</div>
									</div>
									<?php }

								}
								if ( count($rows)==4){
									foreach($rows as $row)
									{
										?>
										<div class=" ffs-four-one ffs-info-box center ">
											<div class="ffs-content-box">
												<?php echo $row['widget_kolom']?>
											</div>
										</div>
										<?php }

									}
								}

								?>
							</div>

						</div>
					</div>
				</div>
			</div>
			<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="container">
					<div class="sixteen columns">
						<div class="site-info">
							<?php fruitful_get_footer_text(); ?>
						</div><!-- .site-info -->
						<?php if (!fruitful_is_social_header()) { 	
							fruitful_get_socials_icon(); 
						} 
						?>
					</div>
				</div>
				<div id="back-top">
					<a rel="nofollow" href="#top" title="Back to top">&uarr;</a>
				</div>
			</footer><!-- #colophon .site-footer -->
			<!--WordPress Development by Fruitful Code-->
			<?php wp_footer(); ?>
		</body>
		</html>