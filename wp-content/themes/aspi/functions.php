<?php
/**
 * Add meta box
 *
 * @param post $post The post object
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/add_meta_boxes
 */
function food_add_meta_boxes( $post ){
	add_meta_box( 'food_meta_box', __( 'Nutrition facts', 'food_example_plugin' ), 'food_build_meta_box', 'food', 'side', 'low' );
}
add_action( 'add_meta_boxes_food', 'food_add_meta_boxes' );


function food_build_meta_box( $post ){
	// our code here
	wp_nonce_field( basename( __FILE__ ), 'food_meta_box_nonce' );
}
