<?php get_header(); ?>

<h1><?php the_title(); ?></h1>

<form>
	<div class="form-group">
		<label for="exampleInputEmail1">Nama Peserta</label>
		<input type="text" class="form-control"  placeholder="Enter nama">

	</div>
	<div class="form-group">
		<label for="alamat">Alamat</label>
		<textarea class="form-control" id="alamat" rows="3"></textarea>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1">Email address</label>
		<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
		<small id="emailHelp" class="form-text text-muted">masukkan email unik aktif anda</small>
	</div>
	<fieldset class="form-group">
		<legend>Hotel</legend>
		<?php
		if( have_rows('hotel') ):
			while ( have_rows('hotel') ) : the_row();
		?>
		<div class="form-check">
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="nama-hotel" id="nama-hotel" value="<?php the_sub_field('nama_hotel'); ?>" checked>
				<?php the_sub_field('nama_hotel'); ?>
			</label>
		</div>
		<fieldset class="form-group kamar">
			<legend>Kamar</legend>
			<?php 
			if( have_rows('kamar') ):
				while ( have_rows('kamar') ) : the_row();

			?>
			<div class="form-check">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="kamar" id="kamar" value="<?php the_sub_field('nomor_kamar'); ?>" checked>
					<?php the_sub_field('nomor_kamar'); ?>
				</label>
			</div>
			<?php 
			endwhile;
			else :
				echo "Maaf Tidah Ada kamar yang tersedia";
			endif;
			?>
		</fieldset>
		<?php 
		endwhile;
		else :
			echo "Maaf Tidah Ada hotel yang tersedia";
		endif;
		?>
	</fieldset>

	<div class="form-group">
		<label for="penjemputan">Penjemputan</label>
		<select class="form-control" id="penjemputan">
			<option value="0">Pilih Lokasi</option>
			<?php if(have_rows('penjemputan')):
			while (have_rows('penjemputan')): the_row(); ?>
			<option value="<?php the_sub_field('lokasi_penjemputan'); ?>">
				<?php the_sub_field('lokasi_penjemputan'); ?></option>
			<?php endwhile;
			else:
				echo "tidak tersedia lokasi penjemputan";
			endif;
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="pengantaran">Pengantaran</label>
		<select class="form-control" id="pengantaran">
			<option value="0">Pilih Lokasi</option>
			<?php if(have_rows('pengantaran')):
			while (have_rows('pengantaran')): the_row(); ?>
			<option value="<?php the_sub_field('lokasi_pengantaran'); ?>">
				<?php the_sub_field('lokasi_pengantaran'); ?></option>
			<?php endwhile;
			else:
				echo "tidak tersedia lokasi pengantaran";
			endif;
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="pengantaran">Jenis Kendaraan</label>
		<select class="form-control" id="pengantaran">
			<option value="0">Pilih Kendaraan</option>
			<?php if(have_rows('jenis_kendaraan')):
			while (have_rows('jenis_kendaraan')): the_row(); ?>
			<option value="<?php the_sub_field('kendaraan'); ?>">
				<?php the_sub_field('kendaraan'); ?></option>
			<?php endwhile;
			else:
				echo "tidak tersedia kendaraan";
			endif;
			?>
		</select>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php 

get_footer(); ?>