<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aspi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bRIWR{il+UMcINEBn53V_?^bP^o^VPVx(gB5%*IFi+DzPs,8Z_hW+-h+QZqc_q_r');
define('SECURE_AUTH_KEY',  'wO2n| -frl{z?o6--#& TEE;%]Y5!9z-|@B:KorEo{Be1>+JP-)$Lz5fd*^ jM[F');
define('LOGGED_IN_KEY',    '(wH.kW+RrX))8g k3.ct0+KZSF:&<EdbVAp$0Vt.ulQQ]^$%`x<XbH`+LF_++.##');
define('NONCE_KEY',        '5iNTu:dfH;hn-W]g o6JMZZ;Z{,~6@3.|AI0ftLh[G|o$wxAzx|Ou8$ZC7ae^#QD');
define('AUTH_SALT',        '/{F7=,B) BYg5#R>8I:wDRaks@)fMebJA O1l#f-D(eL.E1cO`|)lnRe% b/|`d@');
define('SECURE_AUTH_SALT', '?s2c Ku~sD|O%=k+*TG-H=~+(K0$)N1HkqSUq}WRQD6n>Zy /pm_s||ImxrAAu}=');
define('LOGGED_IN_SALT',   'N-M[~/PFp&$j%O+6K#E)K]IY_+=1qCnJM.fR0hqV%c;is#uZ|L,H})zIr]J$ctU*');
define('NONCE_SALT',       'Eb1Q[&S~~^;|^,]/AF-?FVCLjoD`t3~t2<&U$Gs:O]OIL:(oM]?c0.=bDAIq>v`F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_aspi';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
